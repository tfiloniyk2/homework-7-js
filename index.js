function arrToList(array) {
    const newArray = array.map( (element)=> `<li>${element}</li>`)
    const ul = document.createElement('ul');
    ul.insertAdjacentHTML('afterbegin', newArray.join('') )
    document.body.append(ul)
  
    
  }
  
  const testArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  arrToList(testArray);
  
  
  